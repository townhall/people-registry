FROM ruby:2.4.2
MAINTAINER Francesco Boffa <fra.boffa@gmail.com>

RUN mkdir /app
WORKDIR /app

ADD Gemfile Gemfile.lock /app/
RUN bundle install --jobs 4

ADD . /app
CMD ["puma"]
