class CreatePeople < ActiveRecord::Migration[5.1]
  def change
    create_table :people, id: :uuid do |t|
      t.string :first_name, null: false
      t.string :last_name, null: false
      t.date :date_of_birth, null: false
      t.date :date_of_death

      t.timestamps
    end
  end
end
