FactoryBot.define do
  factory :person do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    date_of_birth { Faker::Date.birthday(0, 110) }
    date_of_death nil

    trait :dead do
      date_of_death { Faker::Date.between(date_of_birth, Date.today) }
    end
  end
end
